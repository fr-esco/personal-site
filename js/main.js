import '../tmp/main.bundle.css';
import '../css/cookie-bar.css';

import './pwa.js';
// import './plugins.js';

function transitions() {
  var wf = document.createElement('style');
  wf.textContent = `button,
  #main-actions a {
    transition: all 0.1s ease-out;
  }

  #qr-container-top {
    transition: transform 0.3s ease-out;
  }

  #profiles a,
  .fa-li {
    transition: color 0.3s ease-out;
  }`;
  var s = document.getElementsByTagName('style')[0];
  s.parentNode.insertBefore(wf, s);
}

// https://developers.google.com/web/fundamentals/performance/lazy-loading-guidance/images-and-video
window.addEventListener("load", function () {
  var lazyPictures = [].slice.call(document.querySelectorAll("picture.lazy"));
  lazyPictures.forEach(function (lazyPicture) {
    lazyPicture.childNodes.forEach(function (node) {
      if (node.tagName === 'SOURCE') {
        node.srcset = node.dataset.srcset;
      } else {
        node.src = node.dataset.src;
      }
    })

    lazyPicture.classList.remove("lazy");
  });

  if ('requestAnimationFrame' in window) {
    window.requestAnimationFrame(transitions);
  } else {
    setTimeout(transitions);
  }
});

document.addEventListener('DOMContentLoaded', function (event) {
  // console.log('DOMContentLoaded');

  document.getElementById('save-contact').addEventListener('click', function () {
    ga('send', 'event', 'Contacts', 'download', 'VCard');
  }, false);

  if ('onbeforeprint' in window) {
    window.onbeforeprint = trackPrint;
  } else {
    window.matchMedia('print').addListener(function (mql) {
      if (mql.matches)
        trackPrint();
    });
  }

  document.getElementById('print-button').addEventListener('click', function () {
    print();
  }, false);

  document.getElementById('show-qr').addEventListener('click', function () {
    qrContainer().classList.remove('visuallyhidden');
    var qrContainerStyle = qrContainer().style;
    var transform = qrContainerStyle.transform;
    if (transform !== 'scaleY(1)') {
      ga('send', 'event', 'Contacts', 'scan', 'QR Code');
      qrContainerStyle.transform = 'scaleY(1)';
    } else
      qrContainerStyle.transform = 'scaleY(0)';
  }, false);

  document.addEventListener('click', function (event) {
    try {
      return event.target.id === 'show-qr'
        || event.target.parentElement.id === 'show-qr'
        || event.target.parentElement.parentElement.id === 'show-qr'
        || closeQr();
    } catch (e) {
      closeQr();
    }
  }, false);
  document.addEventListener('keydown', function (event) {
    return event.key === 'Escape' && closeQr();
  }, false);

  document.getElementById('js-name-copy').addEventListener('click', function () {
    ga('send', 'event', 'Contacts', 'copy', 'JS Name');
    copyTextToClipboard(document.getElementById('js-name').textContent);
  }, false);

  ['github', 'gravatar', 'stackoverflow', 'linkedin', 'devto'].map(function (profileName) {
    document.getElementById('profile-' + profileName + '-link').addEventListener('click', function (event) {
      trackProfile(event, profileName);
    }, false);
  });

  function trackPrint() {
    ga('send', 'event', 'Contacts', 'print', 'Print');
  }

  function trackProfile(event, profileName) {
    var url = event.currentTarget.href;
    if (url) {
      var gaEventObject = {
        eventCategory: 'Contacts',
        eventAction: 'social',
        eventLabel: profileName
      };
      if ('sendBeacon' in navigator) {
        gaEventObject.transport = 'beacon';
      } else {
        event.preventDefault();
        gaEventObject.hitCallback = createFunctionWithTimeout(function () {
          window.open(event.currentTarget.href, '_blank');
        });
      }
      ga('send', 'event', gaEventObject);
    }
  }

  function createFunctionWithTimeout(callback, opt_timeout) {
    var called = false;
    function fn() {
      if (!called) {
        called = true;
        callback();
      }
    }
    setTimeout(fn, opt_timeout || 1000);
    return fn;
  }

  function qrContainer() {
    return document.getElementById('qr-container-top');
  }

  function closeQr() {
    qrContainer().style.transform = 'scaleY(0)';
  }

  function fallbackCopyTextToClipboard(text) {
    var textArea = document.createElement("textarea");
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
      var successful = document.execCommand('copy');
      var msg = successful ? 'successful' : 'unsuccessful';
      console.log('Fallback: Copying text command was ' + msg);
    } catch (err) {
      console.error('Fallback: Oops, unable to copy', err);
    }

    document.body.removeChild(textArea);
  }

  function copyTextToClipboard(text) {
    if (!navigator.clipboard) {
      fallbackCopyTextToClipboard(text);
      return;
    }
    navigator.clipboard.writeText(text).then(function () {
      console.log('Async: Copying to clipboard was successful!');
    }, function (err) {
      console.error('Async: Could not copy text: ', err);
    });
  }

});

// TODO: add service worker code here
if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('./service-worker.js')
    .then(
      function () { console.log('Service Worker Registered'); },
      function (e) { console.error('Service Worker NOT Registered', e); }
    );
}
