var deferredPrompt;

window.addEventListener('beforeinstallprompt', function (e) {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  // Update UI notify the user they can add to home screen
  showPwaButton();
});

document.addEventListener('DOMContentLoaded', function (event) {
  getPwaButton().addEventListener('click', function () {
    if (!deferredPrompt) return false;

    // hide our user interface that shows our A2HS button
    hidePwaButton();
    // Show the prompt
    deferredPrompt.prompt();
    ga('send', 'event', 'a2hs', 'prompt', 'show');
    // Wait for the user to respond to the prompt
    deferredPrompt.userChoice
      .then(function (choiceResult) {
        ga('send', 'event', 'a2hs', 'prompt', choiceResult.outcome);
        if (choiceResult.outcome === 'accepted') {
          console.log('User accepted the A2HS prompt');
        } else {
          console.log('User dismissed the A2HS prompt');
        }
        deferredPrompt = null;
      });
  }, false);
});
/*
var open = false;
window.addEventListener('keydown', function (event) {
  if (event.ctrlKey && event.key === 'm') {
    if (open)
      hidePwaButton()
    else
      showPwaButton()
    open = !open
  }
});
*/
window.addEventListener('appinstalled', function (evt) {
  ga('send', 'event', 'a2hs', 'install');
});

function getPwaButton() {
  return document.getElementById('pwa-button');
}

function hidePwaButton() {
  getPwaButton().style.opacity = '0';
  setTimeout(function () { getPwaButton().style.display = 'none'; }, 200);
}

function showPwaButton() {
  getPwaButton().style.display = 'block';
  setTimeout(function () { getPwaButton().style.opacity = '1'; }, 200);
}
