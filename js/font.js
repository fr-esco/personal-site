function applyFonts() {
  if ('requestAnimationFrame' in window) {
    window.requestAnimationFrame(_applyFonts);
  } else {
    setTimeout(_applyFonts);
  }
}

function _applyFonts() {
  document.body.style.fontFamily = "'Work Sans', sans-serif";
  var titles = document.querySelectorAll('h1, h2');
  for (var title of titles) {
    title.style.fontFamily = "'Taviraj', serif";
  }
}

// https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/webfont-optimization?hl=it
if (typeof FontFace !== 'undefined') {
  console.log('FontFace');
  var titleFont = new FontFace("Taviraj", "url(https://fonts.gstatic.com/s/taviraj/v3/ahccv8Cj3ylylTXzRDYPR-5RgA.woff2)", {
    style: 'normal',
    unicodeRange: 'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD',
    weight: '600'
  });
  var bodyFont = new FontFace("Work Sans", "url(https://fonts.gstatic.com/s/worksans/v3/QGYsz_wNahGAdqQ43Rh_fKDp.woff2)", {
    style: 'normal',
    unicodeRange: 'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD',
    weight: '400'
  });

  Promise.all([
    bodyFont.load(),
    titleFont.load()
  ])
    .then(function () {
      document.fonts.add(titleFont);
      document.fonts.add(bodyFont);
      applyFonts();
    });
} else {
  console.warn('WebFont');
  window.WebFontConfig = {
    google: { families: ['Taviraj:600|Work+Sans'] }
  };

  (function () {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    // wf.async = true;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();
  applyFonts();
}
