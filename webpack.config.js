const path = require('path');

module.exports = {
  // mode: 'development',
  // devtool: 'eval-source-map', // https://webpack.js.org/configuration/devtool/
  devtool: 'nosources-source-map',
  entry: {
    main: './js/main.js',
    font: './js/font.js'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.join(path.resolve(__dirname), 'public')
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }
    ]
  },
  plugins: [
  ]
};
