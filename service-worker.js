var dataCacheName = 'fcolamonici-v1.3.1';
var cacheName = 'fcolamonici-v1.3.1';
var filesToCache = [
  '/',
  '/404.html',
  '/favicon-inverted-48.png',
  '/favicon-inverted-96.png',
  '/favicon-inverted-144.png',
  '/favicon-inverted-192.png',
  '/favicon-inverted-240.png',
  '/favicon-inverted-288.png',
  '/favicon-inverted-336.png',
  '/favicon-inverted-384.png',
  '/favicon-inverted-432.png',
  '/favicon-inverted-480.png',
  '/favicon-inverted-512.png',
  '/favicon-inverted-528.png',
  '/favicon-inverted.ico',
  '/favicon-inverted.svg',
  '/favicon.ico',
  '/favicon.svg',
  '/font.bundle.js',
  '/francesco.colamonici.vcf',
  '/index.html',
  '/main.bundle.js',
  '/service-worker.js',
  '/site.webmanifest',
  '/css/cookie-bar.css',
  '/css/main.bundle.css',
  '/css/transitions.css',
  '/img/Francesco_Colamonici.png',
  '/img/Francesco_Colamonici.svg',
];

// https://stackoverflow.com/questions/39432717/how-can-i-cache-external-urls-using-service-worker#answer-42324674
var externalToCache = [
  'https://www.gravatar.com/avatar/82d353ae5a09599ac19268955838fa94?s=280&d=identicon',
  'https://fonts.gstatic.com/s/worksans/v3/QGYsz_wNahGAdqQ43Rh_fKDp.woff2',
  'https://fonts.gstatic.com/s/taviraj/v3/ahccv8Cj3ylylTXzRDYPR-5RgA.woff2',
  'https://fonts.googleapis.com/css?family=Taviraj:600|Trirong:400,400i',
  // new Request('https://www.google-analytics.com/analytics.js', { mode: 'no-cors' }),
  // 'https://cdn.jsdelivr.net/npm/cookie-bar/cookiebar-latest.min.js?theme=flying&tracking=1&thirdparty=1&showNoConsent=1&noGeoIp=1',
  // 'https://cdn.jsdelivr.net/npm/cookie-bar/themes/cookiebar-flying.min.css',
  // 'https://cdn.jsdelivr.net/npm/cookie-bar/lang/it.html',
  // 'https://cdn.jsdelivr.net/npm/cookie-bar/lang/en.html',
];

self.addEventListener('install', function (e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function (cache) {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache)
        .then(function () {
          console.log('[ServiceWorker] Local files cached');
        }, function (error) {
          console.error('[ServiceWorker] Local files caching error', error);
        })
        .then(function () {
          return cache.addAll(externalToCache)
            .then(function () {
              console.log('[ServiceWorker] External files cached');
            }, function (error) {
              console.error('[ServiceWorker] External files caching error', error);
            });
        });
    })
  );
});

self.addEventListener('activate', function (e) {
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    caches.keys().then(function (keyList) {
      return Promise.all(keyList.map(function (key) {
        if (key !== cacheName && key !== dataCacheName) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
  /*
   * Fixes a corner case in which the app wasn't returning the latest data.
   * You can reproduce the corner case by commenting out the line below and
   * then doing the following steps: 1) load app for first time so that the
   * initial New York City data is shown 2) press the refresh button on the
   * app 3) go offline 4) reload the app. You expect to see the newer NYC
   * data, but you actually see the initial data. This happens because the
   * service worker is not yet activated. The code below essentially lets
   * you activate the service worker faster.
   */
  return self.clients.claim();
});

self.addEventListener('fetch', function (e) {
  console.log('[Service Worker] Fetch', e.request.url);
  var dataUrl = 'https://www.google-analytics.com/r/collect';
  if (e.request.url.indexOf(dataUrl) > -1) {
    /*
     * When the request URL contains dataUrl, the app is asking for fresh
     * weather data. In this case, the service worker always goes to the
     * network and then caches the response. This is called the "Cache then
     * network" strategy:
     * https://jakearchibald.com/2014/offline-cookbook/#cache-then-network
     */
    e.respondWith(
      caches.open(dataCacheName).then(function (cache) {
        return fetch(e.request).then(function (response) {
          cache.put(e.request.url, response.clone());
          return response;
        });
      })
    );
  } else {
    /*
     * The app is asking for app shell files. In this scenario the app uses the
     * "Cache, falling back to the network" offline strategy:
     * https://jakearchibald.com/2014/offline-cookbook/#cache-falling-back-to-network
     */
    e.respondWith(
      caches.match(e.request).then(function (response) {
        return response || fetch(e.request);
      })
    );
  }
});
