const fse = require('fs-extra')
const path = require('path')

const files = [
  '404.html',
  'browserconfig.xml',
  'css/cookie-bar.css', // noscript
  'css/transitions.css', // noscript
  'favicon-inverted.ico',
  'favicon-inverted.svg',
  'favicon-inverted-48.png',
  'favicon-inverted-96.png',
  'favicon-inverted-144.png',
  'favicon-inverted-192.png',
  'favicon-inverted-240.png',
  'favicon-inverted-288.png',
  'favicon-inverted-336.png',
  'favicon-inverted-384.png',
  'favicon-inverted-432.png',
  'favicon-inverted-480.png',
  'favicon-inverted-512.png',
  'favicon-inverted-528.png',
  'favicon.ico',
  'favicon.svg',
  'francesco.colamonici.vcf',
  'google0b6dc402e472bc42.html',
  'humans.txt',
  'img/Francesco_Colamonici.png',
  'img/Francesco_Colamonici.svg',
  'robots.txt',
  'service-worker.js',
  'site.webmanifest'
]

files.forEach(file =>
  fse.copy(file, path.join('public', file))
    .then(() => {
      console.log('Assets copied successfully', file)
    })
    .catch(err => {
      console.error('Error copying Assets', err)
    })
)

fse.copy('tmp/main.bundle.css', path.join('public', 'css', 'main.bundle.css'))
  .then(() => {
    console.log('Assets copied successfully', 'tmp/main.bundle.css')
  })
  .catch(err => {
    console.error('Error copying Assets', err)
  })
