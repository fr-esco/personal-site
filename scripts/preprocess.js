// https://medium.com/douglas-matoso-english/build-static-site-generator-nodejs-8969ebe34b22

const fse = require('fs-extra')
const path = require('path')
const { promisify } = require('util')

const ejsRenderFile = promisify(require('ejs').renderFile)
const globP = promisify(require('glob'))

const { library, dom, icon, layer } = require('@fortawesome/fontawesome-svg-core');
const brands = require('@fortawesome/free-brands-svg-icons').fab;
const solid = require('@fortawesome/free-solid-svg-icons').fas;

library.add(brands)
library.add(solid)

const srcPath = './src'
const distPath = './tmp'

const config = {
  site: {
    title: 'Title',
    description: 'Description',
    profiles: fse.readJsonSync(`${srcPath}/data/profiles.json`)
  }
}

globP('**/*.ejs', { cwd: `${srcPath}/pages` })
  .then((files) => {
    files.forEach((file) => {
      const fileData = path.parse(file)
      const destPath = path.join(distPath, fileData.dir)

      // create destination directory
      fse.mkdirs(destPath)
        .then(() => {
          // render page
          return ejsRenderFile(`${srcPath}/pages/${file}`, Object.assign({}, config, { faIcon, faLayer }))
        })
        .then((pageContents) => {
          // render layout with page contents
          return ejsRenderFile(`${srcPath}/layout.ejs`, Object.assign({}, config, { body: pageContents, faCss }))
        })
        .then((layoutContent) => {
          // save the html file
          fse.writeFile(`${destPath}/${fileData.name}.html`, layoutContent)
        })
        .catch((err) => { console.error(err) })
    })
  })
  .catch((err) => { console.error(err) })


function faIcon(iconName, iconParams) {
  return icon(iconName, iconParams).html
}

function faLayer(icons) {
  return layer(push => {
    icons.map(icons => {
      // console.log('Layering icon', icon)
      push(icon(icons[0], icons[1]))
    })
  }).html
}

function faCss() {
  return dom.css()
}
